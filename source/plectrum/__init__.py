# :coding: utf-8
# :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
# :license: Apache License, Version 2.0. See LICENSE.txt.

from ._version import __version__

import plectrum.api

#: Alias of :func:`plectrum.api.pick_single` for convenience.
pick_single = plectrum.api.pick_single

#: Alias of :func:`plectrum.api.pick_multiple` for convenience.
pick_multiple = plectrum.api.pick_multiple
