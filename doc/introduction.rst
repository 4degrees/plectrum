..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _introduction:

************
Introduction
************

A brief introduction to Plectrum.
