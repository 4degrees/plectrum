..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _api_reference:

*************
API Reference
*************

plectrum
========

.. automodule:: plectrum

.. toctree::
    :maxdepth: 1
    :glob:

    */index
    *
