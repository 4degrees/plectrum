..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _tutorial:

********
Tutorial
********

A quick dive into using Plectrum.
