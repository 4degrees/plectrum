..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

********
Glossary
********

.. glossary::

    Virtualenv
        A tool to create isolated Python environments.

        .. seealso:: https://virtualenv.pypa.io/en/latest/
