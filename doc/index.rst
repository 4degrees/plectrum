..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _main:

########
Plectrum
########

Interactively select items from a list in a terminal.

.. toctree::
    :maxdepth: 1

    introduction
    installing
    tutorial
    api_reference/index
    release/index
    license
    glossary

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
