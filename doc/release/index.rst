..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _release_notes:

***************************
Release and migration notes
***************************

Find out what has changed between versions and see important migration notes to
be aware of when switching to a new version.

.. toctree::
    :maxdepth: 1

    release_notes
    migration_notes
