..
    :copyright: Copyright (c) 2017 Martin Pengelly-Phillips
    :license: Apache License, Version 2.0. See LICENSE.txt.

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.1.0-alpha
    :date: 2017-01-19

    .. change:: new

        Initial release for evaluation. Provides API for interactively picking
        :func:`single <plectrum.pick_single>` or :func:`multiple
        <plectrum.pick_multiple>` items from a list of items.
